package hu.wrd.shapes;
import java.util.Arrays;
import java.util.*;

public class ShapeService {
	
	public Shape objects[] = null;
	
	
	public static Shape[] addelem(int n, Shape arr[], Shape x)
   {
       int i;
       Shape newarr[] = new Shape[n + 1];
       for (i = 0; i < n; i++)
           newarr[i] = arr[i];
   
       newarr[n] = x;
   
       return newarr;
   }
	
    public void addShapes(Shape ... shapes) {
        boolean adding = true;
        Shape tmp[] = objects;
        int i = 0;
        while(shapes.length >= i && adding == true)
        {
			int j = 0;
			boolean good = true;
			while(tmp.length > j && good == true)
			{
				if((shapes[i].Terulet() == tmp[j].Terulet() && shapes[i].Kerulet() == tmp[j].Kerulet()) || shapes[i].NotZero() == false)
					good = false;
				j++;
			}
			if(good == true){
				tmp = addelem(tmp.length, tmp, shapes[i]);
				i++;
			}
			else 
				adding = false;
		}
		if (adding == true)
			objects = tmp;
    }
    
    public void OrderAsc(Shape arr[])
    {
		int pos;
        Shape temp;
        for (int i = 0; i < arr.length; i++) 
        { 
            pos = i; 
            for (int j = i+1; j < arr.length; j++) 
           {
                if (arr[j].Terulet() < arr[pos].Terulet())
                {
                    pos = j;
                }
            }

            temp = arr[pos];
            arr[pos] = arr[i]; 
            arr[i] = temp; 
        }
	}
	
	public void OrderDesc(Shape arr[])
    {
		int pos;
        Shape temp;
        for (int i = 0; i < arr.length; i++) 
        { 
            pos = i; 
            for (int j = i+1; j < arr.length; j++) 
           {
                if (arr[j].Terulet() > arr[pos].Terulet())
                {
                    pos = j;
                }
            }

            temp = arr[pos];
            arr[pos] = arr[i]; 
            arr[i] = temp; 
        }
	}

    public void printShapesOrderByAreaAsc() {
        Shape temp[] = objects;
		OrderAsc(temp);
		for(int j = 0; j < temp.length; j++)
		{
			temp[j].WriteForm();
		}
        System.out.println("printShapesOrderByAreaAsc() called");
    }

    public void printShapesOrderByAreaDesc() {
        Shape temp[] = objects;
		OrderDesc(temp);
		for(int j = 0; j < temp.length; j++)
		{
			temp[j].WriteForm();
		}
        System.out.println("printShapesOrderByAreaDesc() called");
    }
}
